# DuckQuack-build

Sono disponibili le build di DuckQuack per convenienza.  
Non si assume nessuna responsabilità sul funzionamento e né si accetteranno problematiche relative a questi pacchetti.  
La versione della JRE (runtime Java) da utilizzare è la 8. I pacchetti non contengono la JRE.  
Procurarsi una versione di Open JRE come per esempio la Zulu scaricabile da qui: https://www.azul.com/downloads/#zulu  
Le versioni OpenJDK sono più lente delle versioni Oracle (https://www.java.com/en/download/), regolatevi a vostro piacimento.  
I pacchetti sono compressi tar.xz, al di fuori di uno GNU/Linux procuratevi un decompressore in grado di estrarli tipo: https://7-zip.org/  
Scaricata la JRE estraetela in una cartella jre all'interno della struttura del pacchetto per avere una cosa simile:

bin  
config  
db  
doc  
examples  
fxml  
gems  
images  
jre <-- qui  
lib  
log  

La versione OsX è diversa e la struttura sarà dentro:

Contents/Resources  

Buona fortuna.


